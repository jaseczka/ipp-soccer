program PaperSoccer;

{$mode objfpc}{$H+}

uses
    {$IFDEF UNIX}{$IFDEF UseCThreads}
    cthreads,
    {$ENDIF}{$ENDIF}
    Interfaces, // this includes the LCL widgetset
    Forms, GUI, logic, mygraphics, newgamedialog;

{$R *.res}

begin
    RequireDerivedFormResource := True;
    Application.Initialize;
    Application.CreateForm(TPaperSoccerForm, PaperSoccerForm);
    Application.CreateForm(TNewGame, NewGame);
    Application.Run;
end.

