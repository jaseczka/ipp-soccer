unit GUI;

{$mode objfpc}{$H+}

interface

uses
    Classes, SysUtils, FileUtil, Forms, Controls, Graphics, Dialogs, ExtCtrls,
    StdCtrls, ComCtrls, ActnList, logic, mygraphics, newgamedialog;

type

    { TPaperSoccerForm }

  TPaperSoccerForm = class(TForm)
    CloseAction: TAction;
    HelpAction: TAction;
    ChangePlayerAction: TAction;
    RedoSingleAction: TAction;
    UndoSingleAction: TAction;
    UndoAction: TAction;
    RedoAction: TAction;
    SetBallAction: TAction;
    EditBoardAction: TAction;
    NewGameAction: TAction;
    SaveGameAction: TAction;
    LoadGameAction: TAction;
    ActionList: TActionList;
    HelpButton: TButton;
    ScrollBox1: TScrollBox;
    SetBallButton: TButton;
    StatusBar: TStatusBar;
    UndoSingleButton: TButton;
    RedoSingleButton: TButton;
    ChangePlayerButton: TButton;
    UndoMoveButton: TButton;
    EditBoardButton: TButton;
    LoadButton: TButton;
    LoadGameDialog: TOpenDialog;
    SaveButton: TButton;
    CloseButton: TButton;
    NewGameButton: TButton;
    GamePanel: TPanel;
    MenuPanel: TPanel;
    SaveGameDialog: TSaveDialog;
    RedoMoveButton: TButton;
    LatticePoint: TGraphicLatticePoint;
    procedure CloseActionExecute(Sender: TObject);
    procedure EditBoardActionExecute(Sender: TObject);
    procedure HelpActionExecute(Sender: TObject);
    procedure LoadGameActionExecute(Sender: TObject);
    procedure NewGameActionExecute(Sender: TObject);
    procedure CloseButtonClick(Sender: TObject);
    procedure EditBoardButtonClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure GamePanelResize(Sender: TObject);
    procedure HelpButtonClick(Sender: TObject);
    procedure LoadButtonClick(Sender: TObject);
    procedure NewGameButtonClick(Sender: TObject);
    procedure RedoActionExecute(Sender: TObject);
    procedure RedoMoveButtonClick(Sender: TObject);
    procedure RedoSingleActionExecute(Sender: TObject);
    procedure RedoSingleButtonClick(Sender: TObject);
    procedure SaveButtonClick(Sender: TObject);
    procedure SaveGameActionExecute(Sender: TObject);
    procedure SetBallActionExecute(Sender: TObject);
    procedure SetBallButtonClick(Sender: TObject);
    procedure ChangePlayerActionExecute(Sender: TObject);
    procedure ChangePlayerButtonClick(Sender: TObject);
    procedure UndoActionExecute(Sender: TObject);
    procedure UndoMoveButtonClick(Sender: TObject);
    procedure UndoSingleActionExecute(Sender: TObject);
    procedure UndoSingleButtonClick(Sender: TObject);
    procedure PointClick(Sender: TObject);
  private
    Game: TGame;
    Points: Array of Array of TGraphicLatticePoint;
    LastPoint: TGraphicLatticePoint;
    IsEditMode: Boolean;
    IsSetBallMode: Boolean;

    procedure CreateHumanGame;
    procedure CreateHumanGame(bWidth, bHeight, ballRow, ballCol: Integer);
    procedure TryMoveTo(p: TPoint);
    procedure DrawBoard;
    procedure SetupBoard;
    procedure FreeBoard;
    procedure UpdateStatus;
    procedure PositionPoints;
    procedure CheckIfEnded;
  public
      { public declarations }
  end;

var
    PaperSoccerForm: TPaperSoccerForm;

implementation

{$R *.lfm}

{ TPaperSoccerForm }

procedure TPaperSoccerForm.CreateHumanGame;
var
  Board: TBoard;
  Player1: TPlayer;
  Player2: TPlayer;
begin
  Board  := TBoard.Create;
  Player1 := THumanPlayer.Create;
  Player2 := THumanPlayer.Create;
  Game := TGame.Create(Player1, Player2, Board);
end;

procedure TPaperSoccerForm.CreateHumanGame(bWidth, bHeight, ballRow,
  ballCol: Integer);
var
  Board: TBoard;
  Player1: TPlayer;
  Player2: TPlayer;
begin
  Board  := TBoard.Create(bWidth, bHeight, ballRow, ballCol);
  Player1 := THumanPlayer.Create;
  Player2 := THumanPlayer.Create;
  Game := TGame.Create(Player1, Player2, Board);
end;

procedure TPaperSoccerForm.TryMoveTo(p: TPoint);
begin
  if (not Game.CanMoveTo(p.y, p.x)) then exit;

  Game.MakeMove(TBoard.GetDirection(Game.BallRow, Game.BallCol, p.y, p.x));

  DrawBoard;
  UpdateStatus;

  RedoMoveButton.Enabled   := False;
  UndoMoveButton.Enabled   := True;
  RedoSingleButton.Enabled := False;
  UndoSingleButton.Enabled := True;

  CheckIfEnded;
end;

procedure TPaperSoccerForm.DrawBoard;
var
  c: Integer;
  r: Integer;
begin
  for c := 0 to Game.BoardWidth - 1 do
  begin
    for r := 0 to Game.BoardHeight - 1 do
    begin
        Points[r][c].Paint;
    end;
  end;
end;

procedure TPaperSoccerForm.SetupBoard;
var
  c: Integer;
  r: Integer;
begin
  SetLength(Points, Game.BoardHeight, Game.BoardWidth);
  for c := 0 to Game.BoardWidth - 1 do
  begin
    for r := 0 to Game.BoardHeight - 1 do
    begin
      Points[r][c] := TGraphicLatticePoint.Create(Self, Game, c, r);
      Points[r][c].OnClick := @PointClick;
      Points[r][c].Parent  := GamePanel;
    end;
  end;
  PositionPoints;
end;

procedure TPaperSoccerForm.FreeBoard;
var
  c: Integer;
  r: Integer;
begin
  for c := 0 to Game.BoardWidth - 1 do
  begin
    for r := 0 to Game.BoardHeight - 1 do
    begin
      Points[r][c].Free;
    end;
  end;
  SetLength(Points, 0, 0);
end;

procedure TPaperSoccerForm.UpdateStatus;
begin
  if (Game.IsFirstActive)
  then StatusBar.SimpleText := 'Aktywny Gracz: 1. Graj do góry.'
  else StatusBar.SimpleText := 'Aktywny Gracz: 2. Graj w dół.';
end;

procedure TPaperSoccerForm.PositionPoints;
var
  offsetX, offsetY: Integer;
  c: Integer;
  r: Integer;
begin
  offsetX := (GamePanel.Width - Points[0][0].Side * Game.BoardWidth) div 2;
  if (offsetX < 0) then offsetX := 0;
  offsetY := (GamePanel.Height - Points[0][0].Side * Game.BoardHeight) div 2;
  if (offsetY < 0) then offsetY := 0;

  for c := 0 to Game.BoardWidth - 1 do
  begin
    for r := 0 to Game.BoardHeight - 1 do
    begin
      Points[r][c].Left    := offsetX + c * Points[r][c].Side;
      Points[r][c].Top     := offsetY + r * Points[r][c].Side;
    end;
  end;
end;

procedure TPaperSoccerForm.CheckIfEnded;
var
  playerName: Char;
  info: String;
begin
  if (Game.HasEnded) then
  begin
    SaveButton.Enabled       := False;
    EditBoardButton.Enabled  := False;
    UndoMoveButton.Enabled   := False;
    RedoMoveButton.Enabled   := False;
    UndoSingleButton.Enabled := False;
    RedoSingleButton.Enabled := False;
    if (Game.HasFirstWon) then playerName := '1' else playerName := '2';
    info := 'Wygrał Gracz ' + playerName + '.';
    StatusBar.SimpleText := info;
    ShowMessage(info);
  end;
end;

procedure TPaperSoccerForm.FormCreate(Sender: TObject);
begin
  CreateHumanGame;

  UndoSingleButton.Enabled   := False;
  UndoMoveButton.Enabled     := False;
  RedoSingleButton.Enabled   := False;
  RedoMoveButton.Enabled     := False;
  ChangePlayerButton.Enabled := False;
  SetBallButton.Enabled      := False;

  SetupBoard;
  UpdateStatus;

  LastPoint := nil;
  IsEditMode := False;
  IsSetBallMode := False;
end;

procedure TPaperSoccerForm.FormDestroy(Sender: TObject);
begin
  SetLength(Points, 0, 0);
  Game.Free;
end;

procedure TPaperSoccerForm.GamePanelResize(Sender: TObject);
begin
  PositionPoints;
end;

procedure TPaperSoccerForm.HelpButtonClick(Sender: TObject);
var
  HelpStrList: TStringList;
begin
  HelpStrList := TStringList.Create;
  try
    HelpStrList.Loadfromfile('pomoc.txt');
    ShowMessage(AnsiToUtf8(HelpStrList.text));
  finally
    HelpStrList.Free;
  end;
end;

procedure TPaperSoccerForm.LoadButtonClick(Sender: TObject);
var
  Player1: TPlayer;
  Player2: TPlayer;
  Board: TBoard;
  NewGame: TGame;
begin
  if LoadGameDialog.Execute then
  begin
    Player1 := THumanPlayer.Create;
    Player2 := THumanPlayer.Create;
    Board := TBoard.Create;
    NewGame := TGame.Create(Player1, Player2, Board);
    if (not NewGame.LoadFromFile(LoadGameDialog.FileName)) then
    begin
      NewGame.Free;
      ShowMessage('Nie można wczytać tego pliku.');
      exit;
    end;
    FreeBoard;
    Game.Free;
    Game := NewGame;
    UndoMoveButton.Enabled   := Game.CanUndo;
    UndoSingleButton.Enabled := Game.CanUndo;
    RedoMoveButton.Enabled   := Game.CanRedo;
    RedoSingleButton.Enabled := Game.CanRedo;
    SetupBoard;
    LastPoint := nil;
    IsEditMode := False;
    DrawBoard;
    UpdateStatus;
  end;
end;

procedure TPaperSoccerForm.CloseButtonClick(Sender: TObject);
begin
  Close;
end;

procedure TPaperSoccerForm.NewGameActionExecute(Sender: TObject);
begin
  if (NewGameButton.Enabled) then NewGameButtonClick(Sender);
end;

procedure TPaperSoccerForm.LoadGameActionExecute(Sender: TObject);
begin
  if (LoadButton.Enabled) then LoadButtonClick(Sender);
end;

procedure TPaperSoccerForm.EditBoardActionExecute(Sender: TObject);
begin
  if (EditBoardButton.Enabled) then EditBoardButtonClick(Sender);
end;

procedure TPaperSoccerForm.CloseActionExecute(Sender: TObject);
begin
  if (CloseButton.Enabled) then CloseButtonClick(Sender);
end;

procedure TPaperSoccerForm.HelpActionExecute(Sender: TObject);
begin
  if (HelpButton.Enabled) then HelpButtonClick(Sender);
end;

procedure TPaperSoccerForm.EditBoardButtonClick(Sender: TObject);
var
  choice: Integer;
begin
  if (IsEditMode) then  { wyjscie z trybu edycji }
  begin
    IsEditMode    := False;
    IsSetBallMode := False;
    if (LastPoint <> nil) then
    begin
      LastPoint.Selected := False;
      LastPoint := nil;
      DrawBoard;
    end;
    EditBoardButton.Caption    := '&Edytuj Planszę';
    ChangePlayerButton.Enabled := False;
    NewGameButton.Enabled      := True;
    SaveButton.Enabled         := True;
    LoadButton.Enabled         := True;
    SetBallButton.Enabled      := False;

    CheckIfEnded;

    exit;
  end;

  choice := MessageDlg('Edycja planszy spowoduje utratę historii gry. ' +
         'Czy chcesz kontynuować?', mtConfirmation, mbOKCancel, 0);
  if (choice = mrCancel) then exit;

  { wejscie w tryb edycji }
  Game.ResetHistory;

  IsEditMode := True;

  EditBoardButton.Caption    := 'Zakończ &Edycję';
  ChangePlayerButton.Enabled := True;
  NewGameButton.Enabled      := False;
  SaveButton.Enabled         := False;
  LoadButton.Enabled         := False;
  UndoMoveButton.Enabled     := False;
  RedoMoveButton.Enabled     := False;
  UndoSingleButton.Enabled   := False;
  RedoSingleButton.Enabled   := False;
  SetBallButton.Enabled      := True;

end;

procedure TPaperSoccerForm.NewGameButtonClick(Sender: TObject);
begin
  if (not NewGame.Execute) then exit;

  FreeBoard;
  Game.Free;

  CreateHumanGame(NewGame.BoardWidth, NewGame.BoardHeight,
      NewGame.BallRow, NewGame.BallCol);

  SaveButton.Enabled       := True;
  EditBoardButton.Enabled  := True;
  UndoMoveButton.Enabled   := False;
  UndoSingleButton.Enabled := False;
  RedoMoveButton.Enabled   := False;
  RedoSingleButton.Enabled := False;
  SetupBoard;
  UpdateStatus;
  LastPoint := nil;
  IsEditMode := False;
end;

procedure TPaperSoccerForm.RedoActionExecute(Sender: TObject);
begin
  if (RedoMoveButton.Enabled) then RedoMoveButtonClick(Sender);
end;

procedure TPaperSoccerForm.RedoMoveButtonClick(Sender: TObject);
begin
  assert(Game.CanRedo);

  Game.RedoMove;

  if (not Game.CanRedo) then
  begin
    RedoMoveButton.Enabled := False;
    RedoSingleButton.Enabled := False;
  end;
  UndoMoveButton.Enabled := True;
  UndoSingleButton.Enabled := True;

  DrawBoard;
  UpdateStatus;
end;

procedure TPaperSoccerForm.RedoSingleActionExecute(Sender: TObject);
begin
  if (RedoSingleButton.Enabled) then RedoSingleButtonClick(Sender);
end;

procedure TPaperSoccerForm.RedoSingleButtonClick(Sender: TObject);
begin
  assert((Game.CanRedo));

  Game.RedoSingle;

  if (not Game.CanRedo) then
  begin
    RedoMoveButton.Enabled := False;
    RedoSingleButton.Enabled := False;
  end;
  UndoSingleButton.Enabled := True;
  UndoMoveButton.Enabled := True;

  DrawBoard;
  UpdateStatus;
end;

procedure TPaperSoccerForm.SaveButtonClick(Sender: TObject);
begin
  if SaveGameDialog.Execute then
  begin
    Game.SaveToFile(SaveGameDialog.FileName);
  end;
end;

procedure TPaperSoccerForm.SaveGameActionExecute(Sender: TObject);
begin
  if (SaveButton.Enabled) then SaveButtonClick(Sender);
end;

procedure TPaperSoccerForm.SetBallActionExecute(Sender: TObject);
begin
  if (SetBallButton.Enabled) then SetBallButtonClick(Sender);
end;

procedure TPaperSoccerForm.SetBallButtonClick(Sender: TObject);
begin
  IsSetBallMode := True;
  SetBallButton.Enabled := False;
end;

procedure TPaperSoccerForm.ChangePlayerActionExecute(Sender: TObject);
begin
  if (ChangePlayerButton.Enabled) then ChangePlayerButtonClick(Sender);
end;

procedure TPaperSoccerForm.ChangePlayerButtonClick(Sender: TObject);
begin
  assert(IsEditMode);

  Game.ChangePlayer;
  DrawBoard;
  UpdateStatus;
end;

procedure TPaperSoccerForm.UndoActionExecute(Sender: TObject);
begin
  if (UndoMoveButton.Enabled) then UndoMoveButtonClick(Sender);
end;

procedure TPaperSoccerForm.UndoMoveButtonClick(Sender: TObject);
begin
  assert(Game.CanUndo);

  Game.UndoMove;

  RedoMoveButton.Enabled   := True;
  RedoSingleButton.Enabled := True;

  if (not Game.CanUndo) then
  begin
    UndoMoveButton.Enabled   := False;
    UndoSingleButton.Enabled := False;
  end;

  DrawBoard;
  UpdateStatus;
end;

procedure TPaperSoccerForm.UndoSingleActionExecute(Sender: TObject);
begin
  if (UndoSingleButton.Enabled) then UndoSingleButtonClick(Sender);
end;

procedure TPaperSoccerForm.UndoSingleButtonClick(Sender: TObject);
begin
  assert(Game.CanUndo);

  Game.UndoSingle;

  RedoMoveButton.Enabled   := True;
  RedoSingleButton.Enabled := True;

  if (not Game.CanUndo) then
  begin
    UndoMoveButton.Enabled   := False;
    UndoSingleButton.Enabled := False;
  end;

  DrawBoard;
  UpdateStatus;
end;

procedure TPaperSoccerForm.PointClick(Sender: TObject);
var
  p: TGraphicLatticePoint;
begin
  p := TGraphicLatticePoint(Sender);

  if (not IsEditMode) then
  begin
    TryMoveTo(p.Pos); { ustawienie przyciskow w TryMoveTo}
    exit;
  end;

  { tryb edycji }
  if (IsSetBallMode) then
  begin
    if (not Game.MoveBall(p.Pos.y, p.Pos.x))
    then ShowMessage('Piłki nie można umieszczać poza planszą, ani w bramkach.');
    SetBallButton.Enabled := True;
    IsSetBallMode := False;
    DrawBoard;
    exit;
  end;

  if (LastPoint = nil) then
  begin
    LastPoint := p;
    p.Selected := True;
    DrawBoard;
    exit;
  end;

  if (LastPoint = p) then
  begin
    LastPoint := nil;
    p.Selected:= False;
    DrawBoard;
    exit;
  end;

  if (not Game.AddSegment(LastPoint.Pos.y, LastPoint.Pos.x, p.Pos.y, p.Pos.x))
  then {nie udało się dodać = segment istnieje => spróbujmy usunąć}
     Game.RemoveSegment(LastPoint.Pos.y, LastPoint.Pos.x, p.Pos.y, p.Pos.x);

  LastPoint.Selected := False;
  LastPoint := p;
  LastPoint.Selected := True;
  DrawBoard;
end;

end.

