unit logic;

{$mode objfpc}{$H+}

interface

uses
    Classes, SysUtils, typinfo;

type
  TDirection = (N, NE, E, SE, S, SW, W, NW, Error);
  TDirectionSet = Set of TDirection;

  { TBall }

  TBall = class
  private
    pRow: Integer;
    pCol: Integer;
  public
    constructor Create(row, col: Integer);
    property Row: Integer read pRow write pRow;
    property Col: Integer read pCol write pCol;
  end;

  { TLatticePoint }

  TLatticePoint = class
  private
    var
    pSegmentSet: TDirectionSet;
  public
    function Add(direction: TDirection): Boolean;
    procedure Add(directions: TDirectionSet);
    function Remove(direction: TDirection): Boolean;
    procedure Remove(directions: TDirectionSet);
    procedure SetNBorder;
    procedure SetEBorder;
    procedure SetSBorder;
    procedure SetWBorder;
    procedure SetFull;
    procedure SetEmpty;
  end;

  { TBoard }

  TBoard = class
  private
    pWidth: Integer;
    pHeight: Integer;
    pPoints: Array of Array of TLatticePoint;
    pBorder: Array of Array of TLatticePoint;
    pBall: TBall;

  public
    class procedure ChangePosition(var row, col: Integer; direction: TDirection);
    class function OppositeDirection(direction: TDirection): TDirection;
    class function GetDirection(startR, startC, endR, endC: Integer): TDirection;

    constructor Create;
    constructor Create(width, height: Integer);
    constructor Create(width, height, ballRow, ballCol: Integer);
    destructor Destroy; override;
    function Move(direction: TDirection): Boolean;
    function EraseMove(direction: TDirection): Boolean;
    function IsSegmentSetEmpty: Boolean;
    function IsLegalMove(direction: TDirection): Boolean;
    function CanMove: Boolean;
    function IsGoalUp: Boolean;
    function IsGoalDown: Boolean;
    procedure SaveToFile(var saveFile: TextFile);
    function LoadSegmentsFromFile(var saveFile: TextFile): Boolean;
    function Add(x, y, dNo: Integer): Boolean;
    function Add(row, col: Integer; d: TDirection): Boolean;
    function Remove(row, col: Integer; d: TDirection): Boolean;
    function IsBorder(row, col: Integer; d:TDirection):Boolean;
  end;

  { TMove }

  TMove = class
  private
    pHistory: TList;
    pLast: Integer;
    function fIsEmpty: Boolean;
    function GetLast: TDirection;
  public
    constructor Create;
    destructor Destroy; override;
    procedure Add(elem: TDirection);
    procedure Undo;
    procedure Redo;
    property Last: TDirection read GetLast;
    property IsEmpty: Boolean read fIsEmpty;
    function CanUndo: Boolean;
    function CanRedo: Boolean;
    procedure Clear;
    procedure ClearFuture;
    procedure Print;
  end;

  { TPlayer }

  TPlayer = class
  private
    pIsHuman: Boolean;
  protected
    constructor Create(isHuman: Boolean);
  public
    property IsHuman: Boolean read pIsHuman;
    function GetMove(board: TBoard): TMove; virtual; abstract;
  end;

  { TComputerPlayer }

  TComputerPlayer = class(TPlayer)
  public
    constructor Create;
    function GetMove(board: TBoard): TMove; override;
  end;

  { THumanPlayer }

  THumanPlayer = class(TPlayer)
  public
    constructor Create;
    function GetMove(board: TBoard): TMove; override;
  end;

  { THistory }

  THistory = class
  private
    pHistory: TList;
    pLast: Integer;
    function fIsEmpty: Boolean;
    function GetLast: TMove;
  public
    constructor Create;
    destructor Destroy; override;
    procedure Add(var elem: TMove);
    procedure Undo;
    procedure Redo;
    property Last: TMove read GetLast;
    property IsEmpty: Boolean read fIsEmpty;
    function CanUndo: Boolean;
    function CanRedo: Boolean;
    procedure Clear;
    procedure ClearFuture;
  end;

  { TGame }

  TGame = class
  private
    pIsFirstActive: Boolean;
    pPlayer1: TPlayer;
    pPlayer2: TPlayer;
    pBoard: TBoard;
    pHistory: THistory;

    function fCanRedo: Boolean;
    function fCanUndo: Boolean;
    function fCanFinishMove: Boolean;
    function LoadHistory(var saveFile: TextFile): Boolean;
    function fGetSegmentSet(r, c: Integer): TDirectionSet;
    function fGetBallCol: Integer;
    function fGetBallRow: Integer;
    function fCanBounceBall: Boolean;
    function fHasEnded: Boolean;
  public
    constructor Create(var player1, player2: TPlayer; var board: TBoard);
    destructor Destroy; override;
    function MakeMove(d: TDirection): Boolean;
    function UndoMove: Boolean;
    function UndoSingle: Boolean;
    function RedoMove: Boolean;
    function RedoSingle: Boolean;
    procedure ResetHistory;
    procedure DeleteFuture;
    procedure ChangeBoard(var newBoard: TBoard);
    function HasFirstWon: Boolean;
    procedure SaveToFile(name: String);
    function LoadFromFile(name: String): Boolean;
    function CanMoveTo(row, col: Integer): Boolean;
    function FinishMove: Boolean;
    function AddSegment(row1, col1, row2, col2: Integer): Boolean;
    function RemoveSegment(row1, col1, row2, col2: Integer): Boolean;
    function MoveBall(row, col: Integer): Boolean;
    procedure ChangePlayer;

    property HasEnded: Boolean read fHasEnded;
    property CanRedo: Boolean read fCanRedo;
    property CanUndo: Boolean read fCanUndo;
    property CanFinishMove: Boolean read fCanFinishMove;
    property BoardWidth: Integer read pBoard.pWidth;
    property BoardHeight: Integer read pBoard.pHeight;
    property BallCol: Integer read fGetBallCol;
    property BallRow: Integer read fGetBallRow;
    property SegmentSet[r, c: Integer]: TDirectionSet
                      read fGetSegmentSet;
    property CanBounceBall: Boolean read fCanBounceBall;
    property IsFirstActive: Boolean read pIsFirstActive;
  end;

implementation

uses
    ai;

{ TMove }

function TMove.fIsEmpty: Boolean;
begin
  fIsEmpty := (pHistory.Count = 0);
end;

function TMove.GetLast: TDirection;
begin
  if pLast >= 0 then
    GetLast := TDirection(Integer(pHistory[pLast]))
  else
    GetLast := Error;
end;

constructor TMove.Create;
begin
  pHistory := TList.Create;
  pLast := -1;
end;

destructor TMove.Destroy;
begin
  pHistory.Free;
  inherited Destroy;
end;

procedure TMove.Add(elem: TDirection);
begin
  { jesli sa zapisane ruchy wprzod nalezy je usunac }
  ClearFuture;
  pHistory.Add(Pointer(Integer(elem)));
  Inc(pLast);
end;

procedure TMove.Undo;
begin
  Dec(pLast);
end;

procedure TMove.Redo;
begin
  Inc(pLast);
end;

function TMove.CanUndo: Boolean;
begin
  CanUndo := (pLast >= 0);
end;

function TMove.CanRedo: Boolean;
begin
  CanRedo := (pLast < pHistory.Count - 1);
end;

procedure TMove.Clear;
begin
  pHistory.Clear;
  pLast := -1;
end;

procedure TMove.ClearFuture;
begin
  pHistory.Count := pLast + 1;
end;

procedure TMove.Print;
var
  i: Integer;
begin
  for i := 0 to pLast do
  begin
    write(GetEnumName(TypeInfo(TDirection), Ord(TDirection(Integer(pHistory[i])))) + ' ');
  end;
  writeln();
end;

{ THistory }

function THistory.fIsEmpty: Boolean;
begin
  fIsEmpty := (pHistory.Count = 0);
end;

function THistory.GetLast: TMove;
begin
  if pLast >= 0 then
    GetLast := TMove(pHistory[pLast])
  else
    GetLast := nil;
end;

constructor THistory.Create;
begin
  pHistory := TList.Create;
  pLast := -1;
end;

destructor THistory.Destroy;
var
  i: Integer;
begin
  for i := 0 to pHistory.Count - 1 do
  begin
    TMove(pHistory[i]).Free;
  end;
  pHistory.Free;
  inherited Destroy;
end;

procedure THistory.Add(var elem: TMove);
begin
  { jesli sa zapisane ruchy wprzod nalezy je usunac }
  ClearFuture;
  pHistory.Add(elem);
  Inc(pLast);
end;

procedure THistory.Undo;
begin
  Dec(pLast);
end;

procedure THistory.Redo;
begin
  Inc(pLast);
end;

function THistory.CanUndo: Boolean;
begin
  CanUndo := (pLast >= 0);
end;

function THistory.CanRedo: Boolean;
begin
  CanRedo := (pLast < pHistory.Count - 1);
end;

procedure THistory.Clear;
begin
  pHistory.Clear;
  pLast := -1;
end;

procedure THistory.ClearFuture;
var
  i: Integer;
begin
  for i := pLast + 1 to pHistory.Count - 1 do
  begin
    TMove(pHistory[i]).Free;
  end;
  pHistory.Count := pLast + 1;
  if (Last <> nil) then Last.ClearFuture;
end;

{ TBall }

constructor TBall.Create(row, col: Integer);
begin
    pRow := row;
    pCol := col;
end;

{ TLatticePoint }

function TLatticePoint.Add(direction: TDirection): Boolean;
begin
  Add := not (direction in pSegmentSet);
  Include(pSegmentSet, direction);
end;

procedure TLatticePoint.Add(directions: TDirectionSet);
begin
  pSegmentSet := pSegmentSet + directions;
end;

function TLatticePoint.Remove(direction: TDirection): Boolean;
begin
  Remove := (direction in pSegmentSet);
  Exclude(pSegmentSet, direction);
end;

procedure TLatticePoint.Remove(directions: TDirectionSet);
begin
  pSegmentSet := pSegmentSet - directions;
end;

procedure TLatticePoint.SetNBorder;
begin
  pSegmentSet := [W, NW, N, NE, E];
end;

procedure TLatticePoint.SetEBorder;
begin
  pSegmentSet := [N, NE, E, SE, S];
end;

procedure TLatticePoint.SetSBorder;
begin
  pSegmentSet := [E, SE, S, SW, W];
end;

procedure TLatticePoint.SetWBorder;
begin
  pSegmentSet := [S, SW, W, NW, N];
end;

procedure TLatticePoint.SetFull;
begin
  pSegmentSet := [N..NW];
end;

procedure TLatticePoint.SetEmpty;
begin
  pSegmentSet := [];
end;

{ TBoard }

class function TBoard.OppositeDirection(direction: TDirection): TDirection;
begin
    case direction of
    N: OppositeDirection := S;
    NE: OppositeDirection := SW;
    E: OppositeDirection := W;
    SE: OppositeDirection := NW;
    S: OppositeDirection := N;
    SW: OppositeDirection := NE;
    W: OppositeDirection := E;
    NW: OppositeDirection := SE;
    end;
end;

class function TBoard.GetDirection(startR, startC, endR, endC: Integer
  ): TDirection;
begin
  if (Abs(startR - endR) > 1) or (Abs(startC - endC) > 1) then
  begin
    GetDirection := Error;
    exit;
  end;

  if (startC < endC) then
  begin
    if      (startR < endR) then GetDirection := SE
    else if (startR = endR) then GetDirection := E
    else                         GetDirection := NE;
  end
  else if (startC = endC) then
  begin
    if      (startR < endR) then GetDirection := S
    else if (startR = endR) then GetDirection := Error
    else                         GetDirection := N;
  end
  else
  begin
    if      (startR < endR) then GetDirection := SW
    else if (startR = endR) then GetDirection := W
    else                         GetDirection := NW;
  end;
end;

constructor TBoard.Create;
const
    WIDTH = 8;
    HEIGHT = 10;
begin
  Create(WIDTH, HEIGHT);
end;

constructor TBoard.Create(width, height: Integer);
begin
  Create(width, height, height div 2, width div 2);
end;

constructor TBoard.Create(width, height, ballRow, ballCol: Integer);
var
    i, j: Integer;
begin
  pWidth := width + 1;
  pHeight := height + 3;
  pBall := TBall.Create(ballRow + 1, ballCol);
  SetLength(pPoints, pHeight, pWidth);
  SetLength(pBorder, pHeight, pWidth);

  { pPoints[RowNo][ColNo] }
  for i := 0 to pHeight - 1 do
  begin
    for j := 0 to pWidth - 1 do
    begin
      pPoints[i][j] := TLatticePoint.Create;
    end;
  end;

  for i := 2 to pHeight - 3 do
  begin
    pPoints[i][0].SetWBorder;
    pPoints[i][pWidth - 1].SetEBorder;
  end;

  for i := 0 to (pWidth div 2) - 2 do
  begin
    pPoints[0][i].SetFull;
    pPoints[0][pWidth - 1 - i].SetFull;
    pPoints[1][i].SetNBorder;
    pPoints[1][pWidth - 1 - i].SetNBorder;
    pPoints[pHeight - 2][i].SetSBorder;
    pPoints[pHeight - 2][pWidth - 1 - i].SetSBorder;
    pPoints[pHeight - 1][i].SetFull;
    pPoints[pHeight - 1][pWidth - 1 - i].SetFull;
  end;

  pPoints[1][0].Add([SW, S]);
  pPoints[1][pWidth - 1].Add([SE, S]);
  pPoints[pHeight - 2][0].Add([NW, N]);
  pPoints[pHeight - 2][pWidth - 1].Add([NE, N]);
  pPoints[0][(pWidth div 2) - 1].SetFull;
  pPoints[0][(pWidth div 2) - 1].Remove(SE);
  pPoints[0][pWidth div 2].SetNBorder;
  pPoints[0][(pWidth div 2) + 1].SetFull;
  pPoints[0][(pWidth div 2) + 1].Remove(SW);
  pPoints[pHeight - 1][(pWidth div 2) - 1].SetFull;
  pPoints[pHeight - 1][(pWidth div 2) - 1].Remove(NE);
  pPoints[pHeight - 1][pWidth div 2].SetSBorder;
  pPoints[pHeight - 1][(pWidth div 2) + 1].SetFull;
  pPoints[pHeight - 1][(pWidth div 2) + 1].Remove(NW);
  pPoints[1][(pWidth div 2) - 1].Add([W, NW, N]);
  pPoints[1][(pWidth div 2) + 1].Add([N, NE, E]);
  pPoints[pHeight - 2][(pWidth div 2) - 1].Add([W, SW, S]);
  pPoints[pHeight - 2][(pWidth div 2) + 1].Add([S, SE, E]);

  for i := 0 to pHeight - 1 do
  begin
    for j := 0 to pWidth - 1 do
    begin
      pBorder[i][j] := TLatticePoint.Create;
      pBorder[i][j].pSegmentSet := pPoints[i][j].pSegmentSet;
    end;
  end;
end;

destructor TBoard.Destroy;
var
  i: Integer;
  j: Integer;
begin
  pBall.Free;
  for i := 0 to pHeight - 1 do
  begin
    for j := 0 to pWidth - 1 do
    begin
      pPoints[i][j].Free;
      pBorder[i][j].Free;
    end;
  end;
  inherited Destroy;
end;

function TBoard.Move(direction: TDirection): Boolean;
var
    r: Integer;
    c: Integer;
begin
  if not IsLegalMove(direction) then
  begin
    Move := False;
    Exit;
  end;
  Move := True;
  pPoints[pBall.Row][pBall.Col].Add(direction);
  r := pBall.Row;
  c := pBall.Col;
  ChangePosition(r, c, direction);
  assert((r >= 0) and (r < pHeight));
  assert((c >= 0) and (c < pWidth));
  pPoints[r][c].Add(OppositeDirection(direction));
  pBall.Row := r;
  pBall.Col := c;
end;

function TBoard.EraseMove(direction: TDirection): Boolean;
var
    r: Integer;
    c: Integer;
begin
  if IsLegalMove(direction) then
  begin
    EraseMove := False;
    Exit;
  end;
  EraseMove := True;
  pPoints[pBall.Row][pBall.Col].Remove(direction);
  r := pBall.Row;
  c := pBall.Col;
  ChangePosition(r, c, direction);
  assert((r >= 0) and (r < pHeight));
  assert((c >= 0) and (c < pWidth));
  pPoints[r][c].Remove(OppositeDirection(direction));
  pBall.Row := r;
  pBall.Col := c;
end;

class procedure TBoard.ChangePosition(var row, col: Integer;
    direction: TDirection);
begin
  case direction of
  N: Dec(row);
  NE:
    begin
      Dec(row);
      Inc(col);
    end;
  E: Inc(col);
  SE:
    begin
      Inc(row);
      Inc(col);
    end;
  S: Inc(row);
  SW:
    begin
      Inc(row);
      Dec(col);
    end;
  W: Dec(col);
  NW:
    begin
      Dec(row);
      Dec(col);
    end;
  end;  { case }
end;

function TBoard.IsSegmentSetEmpty: Boolean;
begin
    IsSegmentSetEmpty := (pPoints[pBall.Row][pBall.Col].pSegmentSet = []);
end;

function TBoard.IsLegalMove(direction: TDirection): Boolean;
begin
  IsLegalMove := ((not (direction = Error)) and
    (not (direction in pPoints[pBall.Row][pBall.Col].pSegmentSet)));
end;

function TBoard.CanMove: Boolean;
begin
  CanMove := (pPoints[pBall.Row][pBall.Col].pSegmentSet <> [N..NW]);
end;

function TBoard.IsGoalUp: Boolean;
begin
  IsGoalUp := ((pBall.Row = 0) and ((pBall.Col = (pWidth div 2) - 1) or
    (pBall.Col = pWidth div 2) or (pBall.Col = (pWidth div 2) + 1)));
end;

function TBoard.IsGoalDown: Boolean;
begin
  IsGoalDown := (pBall.Row = pHeight - 1) and ((pBall.Col = (pWidth div 2) - 1) or
    (pBall.Col = pWidth div 2) or (pBall.Col = (pWidth div 2) + 1));
end;

procedure TBoard.SaveToFile(var saveFile: TextFile);
var
  r: Integer;
  c: Integer;
  d: TDirection;
  tmpBoard: TBoard;
begin
  { wymiary }
  write(saveFile, IntToStr(pWidth - 1));
  write(saveFile, ' ');
  write(saveFile, IntToStr(pHeight - 3));
  writeln(saveFile);
  { pozycja piłki }
  write(saveFile, IntToStr(pBall.Col));
  write(saveFile, ' ');
  write(saveFile, IntToStr(pBall.Row - 1));
  writeln(saveFile);
  { segmenty na planszy }
  tmpBoard := TBoard.Create(pWidth - 1, pHeight - 3);
  for r := 1 to pHeight - 2 do
  begin
    for c := 0 to pWidth - 1 do
    begin
      for d in pPoints[r][c].pSegmentSet do
      begin
        if (tmpBoard.Add(r, c, d)) then
        begin
          write(saveFile, IntToStr(c));
          write(saveFile, ' ');
          write(saveFile, IntToStr(r - 1));
          write(saveFile, ' ');
          write(saveFile, Ord(d));
          write(saveFile, ' | ');
        end;
      end;
    end;
  end;
  tmpBoard.Free;
  writeln(saveFile);
end;

function TBoard.LoadSegmentsFromFile(var saveFile: TextFile): Boolean;
var
  x, y, d: Integer;
  c1, c2, c3: Char;
begin
  while not Eoln(saveFile) do
  begin
    Read(saveFile, x, y, d, c1, c2, c3);
    if (x < 0) or (x >= pWidth) or (y < 0) or (y >= pHeight) or
       (c1 <> ' ') or (c2 <> '|') or (c3 <> ' ') then
    begin
      LoadSegmentsFromFile := False;
      exit;
    end;
    Add(x, y, d);
  end;
  readln(saveFile, c1); { skonsumowanie eol }
  LoadSegmentsFromFile := True;
end;

function TBoard.Add(x, y, dNo: Integer): Boolean;
var
  col: Integer;
  row: Integer;
  d: TDirection;
begin
  col := x;
  row := y + 1;
  d := TDirection(dNo);
  Add := Add(row, col, d);
end;

function TBoard.Add(row, col: Integer; d: TDirection): Boolean;
begin
  if (d = Error) or (d in pPoints[row][col].pSegmentSet) then
  begin
    Add := False;
    exit;
  end;
  pPoints[row][col].Add(d);
  ChangePosition(row, col, d);
  pPoints[row][col].Add(OppositeDirection(d));
  Add := True;
end;

function TBoard.Remove(row, col: Integer; d: TDirection): Boolean;
begin
  if (d = Error) or (not (d in pPoints[row][col].pSegmentSet)) or
    (IsBorder(row, col, d)) then
  begin
    Remove := False;
    exit;
  end;
  pPoints[row][col].Remove(d);
  ChangePosition(row, col, d);
  pPoints[row][col].Remove(OppositeDirection(d));
end;

function TBoard.IsBorder(row, col: Integer; d: TDirection): Boolean;
begin
  IsBorder := (d in pBorder[row][col].pSegmentSet);
end;

{ TComputerPlayer }

constructor TComputerPlayer.Create;
begin
    inherited Create(False);
end;

function TComputerPlayer.GetMove(board: TBoard): TMove;
begin
  GetMove := TAI.GetMove(board);
end;

{ THumanPlayer }

constructor THumanPlayer.Create;
begin
    inherited Create(True);
end;

function THumanPlayer.GetMove(board: TBoard): TMove;
var
    newMove: TMove;
    ch: Char;
begin
  readln(ch);
  newMove := TMove.Create;
  newMove.Add(S);
  GetMove := newMove;
end;

{ TPlayer }

constructor TPlayer.Create(isHuman: Boolean);
begin
    pIsHuman := isHuman;
end;

function TGame.fCanRedo: Boolean;
begin
  assert(pHistory.Last <> nil);

  if (pHistory.Last.CanRedo) then
  begin
    fCanRedo := True;
    exit;
  end;

  {pHistory.Last.CanRedo = False}
  if (not pHistory.CanRedo) then
  begin
    fCanRedo := False;
    exit;
  end;

  {pHistory.CanRedo = True}
  pHistory.Redo;
  fCanRedo := pHistory.Last.CanRedo;
  pHistory.Undo;
end;

function TGame.fCanUndo: Boolean;
begin
  assert(pHistory.Last <> nil);

  if pHistory.pLast > 0 then
  begin
    fCanUndo := True;
    exit;
  end;

  {pHistory.pLast = 0}
  fCanUndo := pHistory.Last.CanUndo;
end;

function TGame.fCanFinishMove: Boolean;
begin
  fCanFinishMove := ((not CanBounceBall) and (pHistory.Last.Last <> Error));
end;

function TGame.LoadHistory(var saveFile: TextFile): Boolean;
var
    dNo: Integer;
    d: TDirection;
    str: String;
    c: Char;
begin
  while not Eof(saveFile) do
  begin
    while not Eoln(saveFile) do
    begin
      read(saveFile, dNo, c);
      if (dNo < 0) or (dNo > 7) then
      begin
        LoadHistory := False;
        exit;
      end;
      d := TDirection(dNo);
      if (not MakeMove(d)) then
      begin
        LoadHistory := False;
        exit;
      end;
    end; { eoln }
    readln(saveFile, str); { skonsumowanie eol }
    FinishMove;
  end; { eof }
  LoadHistory := True;
end;

function TGame.fGetSegmentSet(r, c: Integer): TDirectionSet;
begin
  fGetSegmentSet := pBoard.pPoints[r][c].pSegmentSet;
end;

function TGame.fGetBallCol: Integer;
begin
  fGetBallCol := pBoard.pBall.Col;
end;

function TGame.fGetBallRow: Integer;
begin
  fGetBallRow := pBoard.pBall.Row;
end;

function Count(segmentset: TDirectionSet): Integer;
var
    d: TDirection;
    c: Integer;
begin
  c := 0;
  for d in segmentset do
  begin
    Inc(c);
  end;
  Count := c;
end;

function TGame.fCanBounceBall: Boolean;
begin
  assert(pHistory.Last <> nil);
  fCanBounceBall := ((pHistory.Last.Last <> Error) and
    (not pBoard.IsSegmentSetEmpty) and (Count(SegmentSet[BallRow, BallCol]) > 1));
end;

function TGame.fHasEnded: Boolean;
begin
  fHasEnded := ((not pBoard.CanMove) or (pBoard.IsGoalDown) or
    (pBoard.IsGoalUp));
end;

constructor TGame.Create(var player1, player2: TPlayer; var board: TBoard);
var
  aMove: TMove;
begin
  pPlayer1 := player1;
  pPlayer2 := player2;
  pBoard := board;
  pHistory := THistory.Create;
  aMove := TMove.Create;
  pHistory.Add(aMove);
  pIsFirstActive := True;
end;

destructor TGame.Destroy;
begin
  pPlayer1.Free;
  pPlayer2.Free;
  pBoard.Free;
  pHistory.Free;
  inherited Destroy;
end;

function TGame.MakeMove(d: TDirection): Boolean;
begin
  if (not pBoard.IsLegalMove(d)) then
  begin
    MakeMove := False;
    exit;
  end;

  pBoard.Move(d);

  assert(pHistory.Last <> nil);
  DeleteFuture;
  pHistory.Last.Add(d);

  MakeMove := True;

  if (CanFinishMove) then
  begin
    FinishMove;
  end;
end;

function TGame.UndoMove: Boolean;
begin
  if (not CanUndo) then
  begin
    UndoMove := False;
    exit;
  end;

  assert(pHistory.Last <> nil);

  if (not pHistory.Last.CanUndo) then
  begin
    {jestesmy na poczatku ruchu, ale nie pierwszego skoro da się cofnąć}
    pHistory.Undo;
    assert(pHistory.Last <> nil);
    pIsFirstActive := not pIsFirstActive;
  end;

  while (pHistory.Last.CanUndo) do
  begin
    pBoard.EraseMove(TBoard.OppositeDirection(pHistory.Last.Last));
    pHistory.Last.Undo;
  end;

  UndoMove := True;
end;

function TGame.UndoSingle: Boolean;
begin
  if (not CanUndo) then
  begin
    UndoSingle := False;
    exit;
  end;

  assert(pHistory.Last <> nil);

  if (not pHistory.Last.CanUndo) then
  begin
    pHistory.Undo;
    assert(pHistory.Last <> nil);
    pIsFirstActive := (not pIsFirstActive);
  end;

  pBoard.EraseMove(TBoard.OppositeDirection(pHistory.Last.Last));
  pHistory.Last.Undo;

  UndoSingle := True;
end;

function TGame.RedoMove: Boolean;
begin
  if (not CanRedo) then
  begin
    RedoMove := False;
    exit;
  end;

  assert(pHistory.Last <> nil);

  while (pHistory.Last.CanRedo) do
  begin
    pHistory.Last.Redo;
    pBoard.Move(pHistory.Last.Last);
  end;

  { jesli może ustawia się na początku kolejnego ruchu }
  if (pHistory.CanRedo) then begin
    pHistory.Redo;
    pIsFirstActive := (not pIsFirstActive);
  end;

  RedoMove := True;
end;

function TGame.RedoSingle: Boolean;
begin
  if (not CanRedo) then
  begin
    RedoSingle := False;
    exit;
  end;

  assert(pHistory.Last <> nil);

  if (not pHistory.Last.CanRedo) then
  begin
    pHistory.Redo;
    pIsFirstActive := (not pIsFirstActive);
  end;

  assert(pHistory.Last.CanRedo);

  pHistory.Last.Redo;
  pBoard.Move(pHistory.Last.Last);

  if (not pHistory.Last.CanRedo) and (pHistory.CanRedo) then
  begin
    pHistory.Redo;
    pIsFirstActive := (not pIsFirstActive);
  end;

  RedoSingle := True;
end;

procedure TGame.ResetHistory;
var
    aMove: TMove;
begin
  pHistory.Free;
  pHistory := THistory.Create;
  aMove := TMove.Create;
  pHistory.Add(aMove);
end;

procedure TGame.DeleteFuture;
begin
  pHistory.ClearFuture;
end;

procedure TGame.ChangeBoard(var newBoard: TBoard);
var
    aMove: TMove;
begin
  pBoard.Free;
  pBoard := newBoard;
  pHistory.Clear;
  aMove := TMove.Create;
  pHistory.Add(aMove);
end;

function TGame.HasFirstWon: Boolean;
begin
  HasFirstWon := ((pBoard.IsGoalUp) or
    ((not pIsFirstActive) and (not pBoard.CanMove)));
end;

procedure TGame.SaveToFile(name: String);
var
    saveFile: TextFile;
    pHLast: Integer;
    pHLastLast: Integer;
begin
  pHLast := pHistory.pLast;
  pHLastLast := pHistory.Last.pLast;
  AssignFile(saveFile, name);
  ReWrite(saveFile);

  { aktywny gracz: }
  if (pIsFirstActive)
  then writeln(saveFile, '1')
  else writeln(saveFile, '0');
  { cofniecie historii, aby zapisac segmenty startowe: }
  while (CanUndo) do UndoMove;
  { dane planszy: }
  pBoard.SaveToFile(saveFile);
  { dane historii: }
  while (CanRedo) and (pHistory.pLast < pHLast) do
  begin
    while (pHistory.Last.CanRedo) do
    begin
      pHistory.Last.Redo;
      pBoard.Move(pHistory.Last.Last);
      write(saveFile, IntToStr(Ord(pHistory.Last.Last)) + ' ');
    end;
    writeln(saveFile);
    pHistory.Redo;
  end;

  if (CanRedo) and (pHLastLast > -1) then
  begin
    while (pHistory.Last.CanRedo) and (pHistory.Last.pLast <= pHLastLast) do
    begin
      pHistory.Last.Redo;
      pBoard.Move(pHistory.Last.Last);
      write(saveFile, IntToStr(Ord(pHistory.Last.Last)) + ' ');
    end;
    writeln(saveFile);
  end;

  CloseFile(saveFile);
end;

function TGame.LoadFromFile(name: String): Boolean;
var
    saveFile: TextFile;
    tmp: String;
    width, height: Integer;
    ballX, ballY: Integer;
begin
  try
    AssignFile(saveFile, name);
    Filemode := fmOpenRead;
    Reset(saveFile);
    { aktywny gracz }
    readln(saveFile, tmp);
    if (tmp = '1') then pIsFirstActive := True
    else if (tmp = '0') then pIsFirstActive := False
    else
    begin
      LoadFromFile := False;
      Close(saveFile);
      exit;
    end;
    { rozmiar planszy: }
    readln(saveFile, width, height);
    { pozycja piłki: }
    readln(saveFile, ballX, ballY);
    pBoard.Free;
    pBoard := TBoard.Create(width, height, ballX, ballY);
    { segmenty startowe: }
    if (not pBoard.LoadSegmentsFromFile(saveFile)) then
    begin
      LoadFromFile := False;
      Close(saveFile);
      exit;
    end;
    if (not LoadHistory(saveFile)) then
    begin
      LoadFromFile := False;
      Close(saveFile);
      exit;
    end;
    LoadFromFile := True;
  except
    LoadFromFile := False;
  end;
  Close(saveFile);
end;

function TGame.CanMoveTo(row, col: Integer): Boolean;
begin
  if (not pBoard.IsLegalMove(TBoard.GetDirection(pBoard.pBall.Row,
    pBoard.pBall.Col, row, col))) or (HasEnded) then
  begin
    CanMoveTo := False;
    exit;
  end;

  assert(pHistory.Last <> nil);

  if (pHistory.Last.Last = Error) then
  begin
    CanMoveTo := True;
  end
  else if (CanBounceBall) then
  begin
    CanMoveTo := True;
  end
  else
  begin
    CanMoveTo := False;
  end;
end;

function TGame.FinishMove: Boolean;
var
    aMove: TMove;
begin
  if (not CanFinishMove) then
  begin
    FinishMove := False;
    exit;
  end;

  pIsFirstActive := (not pIsFirstActive);
  aMove := TMove.Create;
  pHistory.Add(aMove);
end;

function TGame.AddSegment(row1, col1, row2, col2: Integer): Boolean;
var
    d: TDirection;
begin
  d := TBoard.GetDirection(row1, col1, row2, col2);
  AddSegment := pBoard.Add(row1, col1, d);
end;

function TGame.RemoveSegment(row1, col1, row2, col2: Integer): Boolean;
var
  d: TDirection;
begin
  d := TBoard.GetDirection(row1, col1, row2, col2);
  RemoveSegment := pBoard.Remove(row1, col1, d);
end;

function TGame.MoveBall(row, col: Integer): Boolean;
begin
  if (row = 0) or (row = pBoard.pHeight - 1) then
  begin
    MoveBall := False;
    exit;
  end;

  pBoard.pBall.Row := row;
  pBoard.pBall.Col := col;
  MoveBall := True;
end;

procedure TGame.ChangePlayer;
begin
  pIsFirstActive := (not pIsFirstActive);
end;

end.
