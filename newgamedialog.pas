unit newgamedialog;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, FileUtil, Forms, Controls, Graphics, Dialogs, StdCtrls,
  ExtCtrls, Spin, Buttons;

type

  { TNewGame }

  TNewGame = class(TForm)
    ConfirmBitBtn: TBitBtn;
    CancelBitBtn: TBitBtn;
    BoardDimensions: TGroupBox;
    BallPosition: TGroupBox;
    HeightLabel: TLabel;
    HeightLabel1: TLabel;
    BallRowSpinEdit: TSpinEdit;
    WidthLabel1: TLabel;
    WidthSpinEdit: TSpinEdit;
    WidthLabel: TLabel;
    HeightSpinEdit: TSpinEdit;
    BallColSpinEdit: TSpinEdit;

    procedure HeightSpinEditEditingDone(Sender: TObject);
    procedure WidthSpinEditEditingDone(Sender: TObject);
  private
    { private declarations }
  public
    BoardHeight: Integer;
    BoardWidth: Integer;
    BallRow: Integer;
    BallCol: Integer;

    function Execute: Boolean;
  end;

var
  NewGame: TNewGame;

implementation

{$R *.lfm}

{ TNewGame }

procedure TNewGame.HeightSpinEditEditingDone(Sender: TObject);
begin
  BallRowSpinEdit.MaxValue := HeightSpinEdit.Value;
  BallRowSpinEdit.Value := HeightSpinEdit.Value div 2;
  HeightSpinEdit.Value := 2 * (HeightSpinEdit.Value div 2);
end;

procedure TNewGame.WidthSpinEditEditingDone(Sender: TObject);
begin
  BallColSpinEdit.MaxValue := WidthSpinEdit.Value;
  BallColSpinEdit.Value := WidthSpinEdit.Value div 2;
  WidthSpinEdit.Value := 2 * (WidthSpinEdit.Value div 2);
end;

function TNewGame.Execute: Boolean;
begin
  Execute      := (ShowModal = mrOK);
  BoardHeight := HeightSpinEdit.Value;
  BoardWidth  := WidthSpinEdit.Value;
  BallRow     := BallRowSpinEdit.Value;
  BallCol     := BallColSpinEdit.Value;
end;

end.

