## Paper Soccer ##
### Final (#4) Individual Programming Homework Project ###

GUI application written with Lazarus IDE.


Paper Soccer Game (human vs. human) features:

* Edit board 
* Undo/Redo move and/or segment
* Save/Load game

Architecture: 2 layers, logic separated from graphics.