unit ai;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, logic;

type

  { TAI }

  TAI = class
  private
    class function AddSegment(var board: TBoard; var move: TMove): Boolean;
  public
    { AI dziala na skopiowanej planszy }
    class function GetMove(board: TBoard): TMove;
  end;

implementation

{ TAI }

class function TAI.AddSegment(var board: TBoard; var move: TMove): Boolean;
var
  d: TDirection;
  success: Boolean;
begin
  success := false;
  for d in [N..NW] do
  begin
    if (board.IsLegalMove(d)) and (not success) then
    begin
      board.Move(d);
      if (board.CanMove) then
        move.Add(d)
      else
        board.Move(TBoard.OppositeDirection(d));
    end;
  end;
  AddSegment := success;
end;

class function TAI.GetMove(board: TBoard): TMove;
var
  move: TMove;
  added: Boolean;
begin
  move := TMove.Create;
  added := True;
  while (not board.IsSegmentSetEmpty) and (added) do  {TODO, to nieprawda}
  begin
    added := AddSegment(board, move);
  end;
  GetMove := move;
end;

end.

