unit mygraphics;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, types, Controls, Graphics, LCLType, logic;

type

  { TGraphicLatticePoint }

  TGraphicLatticePoint = class(TGraphicControl)
  private
    pSide: Integer;
    pPos: TPoint;
    pGame: TGame;
    pSelected: Boolean;

    procedure DrawSegment(Bitmap :TBitmap; d: TDirection);
    procedure DrawPoint(Bitmap: TBitmap);
    procedure SetupBitmap(var Bitmap: TBitmap);
    function HasBall: Boolean;
  public
    constructor Create(AOwner: TComponent; game: TGame;
      posX, posY, side: Integer);
    constructor Create(AOwner: TComponent; game: TGame; posX, posY: Integer);
    procedure Paint; override;

    property Side: Integer read pSide;
    property Pos: TPoint read pPos;
    property Selected: Boolean read pSelected write pSelected;
  end;

implementation

constructor TGraphicLatticePoint.Create(AOwner: TComponent; game: TGame;
  posX, posY, side: Integer);
begin
  inherited Create(AOwner);
  pSide  := side;
  Height := pSide;
  Width  := pSide;
  pPos := Point(posX, posY);
  pGame := Game;
  pSelected := False;
end;

constructor TGraphicLatticePoint.Create(AOwner: TComponent; game: TGame;
  posX, posY: Integer);
begin
  Create(AOwner, game, posX, posY, 30);
end;

procedure TGraphicLatticePoint.Paint;
var
  Bitmap: TBitmap;
  d: TDirection;
begin
  Bitmap := TBitmap.Create;
  try
    SetupBitmap(Bitmap);
    for d in pGame.SegmentSet[pPos.y, pPos.x] do
    begin
      DrawSegment(Bitmap, d);
    end;
    DrawPoint(Bitmap);
    Canvas.Draw(0, 0, Bitmap);
  finally
    Bitmap.Free;
  end;

  inherited Paint;
end;

procedure TGraphicLatticePoint.DrawSegment(Bitmap: TBitmap; d: TDirection);
var
  x1, x2, y1, y2: Integer;
begin
  Bitmap.Canvas.Pen.Color := clWhite;
  Bitmap.Canvas.Pen.Width := 2;
  x1 := Width div 2;
  y1 := Height div 2;
  x2 := x1;
  y2 := y1;

  case d of
  N: y2 := 0;
  NE:
    begin
      x2 := Width;
      y2 := 0;
    end;
  E: x2 := Width;
  SE:
    begin
      x2 := Width;
      y2 := Height;
    end;
  S: y2 := Height;
  SW:
    begin
      x2 := 0;
      y2 := Height;
    end;
  W: x2 := 0;
  NW:
    begin
      x2 := 0;
      y2 := 0;
    end;
  end;  { case }

  Bitmap.Canvas.Line(x1, y1, x2, y2);
end;

procedure TGraphicLatticePoint.DrawPoint(Bitmap: TBitmap);
var
  BallColor: TColor;
begin

  if (Selected) then
  begin
    Bitmap.Canvas.Pen.Color   := clYellow;
    Bitmap.Canvas.Brush.Color := clYellow;
    Bitmap.Canvas.Ellipse((pSide div 2) - 6, (pSide div 2) - 6,
                               (pSide div 2) + 6, (pSide div 2) + 6);
  end;

  if (HasBall) then
  begin
    Bitmap.Canvas.Pen.Color   := clWhite;
    Bitmap.Canvas.Brush.Color := clWhite;
    Bitmap.Canvas.Ellipse((pSide div 2) - 4, (pSide div 2) - 4,
                               (pSide div 2) + 4, (pSide div 2) + 4);

    if (pGame.IsFirstActive) then BallColor := clRed else BallColor := clBlue;

    Bitmap.Canvas.Pen.Color   := BallColor;
    Bitmap.Canvas.Brush.Color := BallColor;
    Bitmap.Canvas.Ellipse((pSide div 2) - 3, (pSide div 2) - 3,
                               (pSide div 2) + 3, (pSide div 2) + 3);
  end
  else
  begin
    Bitmap.Canvas.Pen.Color   := clWhite;
    Bitmap.Canvas.Brush.Color := clWhite;
    Bitmap.Canvas.Ellipse((pSide div 2) - 3, (pSide div 2) - 3,
                               (pSide div 2) + 3, (pSide div 2) + 3);
  end;
end;

procedure TGraphicLatticePoint.SetupBitmap(var Bitmap: TBitmap);
begin
  Bitmap.Height := Height;
  Bitmap.Width := Width;
  Bitmap.Canvas.Pen.Color:=clGreen;
  Bitmap.Canvas.Brush.Color:= clGreen;
  Bitmap.Canvas.Rectangle(0, 0, Width, Height);
end;

function TGraphicLatticePoint.HasBall: Boolean;
begin
  HasBall := ((pPos.x = pGame.BallCol) and (pPos.y = pGame.BallRow));
end;

end.

